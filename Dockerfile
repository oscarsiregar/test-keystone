FROM keystone-test-base:local AS build

WORKDIR /home/node

ADD . /home/node

RUN rm -rf .keystone
RUN rm -rf .tmp
RUN rm yarn.lock

COPY package.json /home/node/

RUN yarn install --verbose

RUN yarn run build

EXPOSE 443 80
CMD ["yarn", "run", "start"]
